package com.sapient.week1.test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class EmployeeDAO 
{
	private List<Employeebean> list;

	public List<Employeebean> readData()
	{
		String s = "C:\\Users\\shrpinja\\project-week1\\src\\com\\sapient\\week1\\test\\";
		FileReader fileReader = null;
		try {
			fileReader = new FileReader(s+"employee.csv");
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
		}
		BufferedReader buffReader = new BufferedReader(fileReader);
		String line;
		list = new ArrayList<Employeebean>();
		try 
		{
			while((line=buffReader.readLine())!=null)
			{
				String data[] = line.split(",");
				int id = Integer.parseInt(data[0]);
				String name = data[1];
				int salary = Integer.parseInt(data[2]);
				Employeebean emp = new Employeebean(id,name,salary);
				list.add(emp);
			}
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		//System.out.println("size : "+list.size());
		return list;
	}
	public double getTotSal(List<Employeebean> list)
	{
		double totalsum = 0;
		
		totalsum = list.stream().mapToDouble(emp->emp.getSalary()).sum();
		
		return totalsum;
	}
	public int getCount(List<Employeebean> list, int salary)
	{
		return (int) list.stream().filter(emp->emp.getSalary()==salary).count();
	}
	
	public Employeebean getEmployee(int id)
	{
		List<Employeebean> templist = list.stream().filter(emp->emp.getId()==id).collect(Collectors.toList());
		return templist.get(0);
	}

	
	
	
}
